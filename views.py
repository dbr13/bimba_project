# -*- coding: utf-8 -*-
import requests

from app import *

from forms import KeyForm, RegistrationForm, LoginForm
from sqlalchemy.exc import IntegrityError
from flask import flash, redirect, render_template, url_for, session, g
from flask_login import login_user, current_user, login_required
from werkzeug.security import generate_password_hash, check_password_hash

import poloniex
import coins_converter
from decorators import requires_user

@app.route('/generate-variables/')
def generate_variables():
    var_all = Variables.query.all()
    for var in var_all:
        db.session.delete(var)
    url = Variables(id=1, name='url', text='https://poloniex.com/public')
    db.session.add(url)
    params = Variables(id=2, name='params', text='{"command": "returnTicker"}')
    db.session.add(params)
    db.session.commit()
    return 'Ok ^_^'


@app.route('/generate-coins/')
def generate_coins():
    coins_all = CoinsConvList.query.all()
    for coin in coins_all:
        db.session.delete(coin)
    r = requests.get('https://poloniex.com/public?command=returnTicker')
    coins_conv_list = r.json().keys()
    for i, c in enumerate(coins_conv_list):
        #if c[:4] != 'USDT':
        coin = CoinsConvList(id=i+1, name=c, enable=True)
        db.session.add(coin)
    db.session.commit()
    return 'Ok ^_^'


@app.route('/generate-convert-usd/')
def generate_convert_usd():
    coins_all = ConvertUsd.query.all()
    for coin in coins_all:
        db.session.delete(coin)
    r = requests.get('https://poloniex.com/public?command=returnTicker')
    usdt_conv_list = r.json().keys()
    for i, u in enumerate(usdt_conv_list):
        if u[:4] == 'USDT':
            coin = ConvertUsd(id=i+1, coins=u, usd=u[5:], enable=True)
            db.session.add(coin)
    db.session.commit()
    return 'Ok ^_^'


@app.route('/generate-users/')
def generate_users():
    users = User.query.all()
    for user in users:
        db.session.delete(user)
    user = User(id=1,
                f_name='Denys',
                l_name='Bortovets',
                email='dober.uk@gmail.com',
                password=generate_password_hash('password'))
    db.session.add(user)
    db.session.commit()
    return 'Ok ^_^'


@app.route('/generate-keys/')
def generate_kyes():
    u_keys = UserKeys.query.all()
    for u_key in u_keys:
        db.session.delete(u_key)
    return 'Ok ^_^'


@app.route('/generate-all/')
def generate_all():
    generate_variables()
    generate_coins()
    generate_convert_usd()
    generate_users()
    generate_kyes()
    return 'Ok ^_^'


@app.route('/')
def calc():
    var_url = Variables.query.filter_by(name='url').first()
    var_params = Variables.query.filter_by(name='params').first()
    url, params = var_url.text, json.loads(var_params.text)
    dict_rates = coins_converter.get_currencies_rates(url=url, params=params)
    
    var_coins_conv_list = CoinsConvList.query.filter_by(enable=True).all()
    coins_conv_list = list(map(lambda x: x.name, var_coins_conv_list))
    var_convert_usd = ConvertUsd.query.filter_by(enable=True).all()
    convert_usd = dict(map(lambda x: [x.usd, x.coins], var_convert_usd))
    
    price = coins_converter.convert_into_usd(var_coins_conv_list=coins_conv_list,
                                             var_convert_usd=convert_usd,
                                             dict_rates=dict_rates)
    rating = coins_converter.coins_rate(price)
    # q_price = calc_quantity(price=price, quantity=amount_of_coins)
    return render_template('/calc.html',
                           rating=rating,
                           #rating=rating,
                           title='calc')


@app.route('/registration', methods=['GET', 'POST'])
def registrate_user():
    registr = RegistrationForm()
    if registr.validate_on_submit():
        u = User(
            f_name=registr.f_name.data,
            l_name=registr.l_name.data,
            email=registr.email.data,
            password=generate_password_hash(registr.password.data)
        )
        db.session.add(u)
        try:
            db.session.commit()
        except IntegrityError as exc:
            err = '{0}'.format(exc.orig)
            flash('Current {0} is already existed. Try another {0}'.format(err.split('.')[-1]))
        else:
            session['user_id'] = u.id
            flash('Registration was successful!')
            return redirect(url_for('login'))
    return render_template('registration.html',
                           title='Registration',
                           registr=registr)


@app.before_request
def before_request():
    """
    pull user's profile from the database before every request are treated
    """
    #g.user = current_user
    g.user = None
    if 'user_id' in session:
        g.user = User.query.get(session['user_id'])


@app.route('/login', methods=['GET', 'POST'])
def login():
    """
    Login Form
    :return:
    """

    log_form = LoginForm()
    if log_form.validate_on_submit():
        user = User.query.filter_by(email=log_form.email.data).first()
        if user and check_password_hash(user.password, log_form.password.data):
            session['user_id'] = user.id
            flash('Welcome - {}'.format(user.f_name))
            return redirect(url_for('add_key'))
        flash('Wrong email or password')
    return render_template('login.html',
                           title='Login',
                           log_form=log_form)


@app.route('/add-key', methods=['GET', 'POST'])
@requires_user
def add_key():
    key = KeyForm()
    if key.validate_on_submit():
        user_keys = UserKeys.query.filter_by(email=key.email.data).all()
        for us_key in user_keys:
            if us_key.secret == key.secret.data:
                flash('Key was activated successfully')
            return redirect(url_for('api_auth'))
        else:
            u_key = UserKeys(
                id=None,
                api_key=key.api_key.data,
                secret=key.secret.data,
                email=key.email.data,
                enable=True,
                user_id=User.query.filter_by(email=key.email.data).first().id
            )
            db.session.add(u_key)
            db.session.commit()
            flash('Key was added successfully and activated')
            return redirect(url_for('api_auth'))
    return render_template('add-key.html',
                           title='AddKey',
                           key=key)


@app.route('/api-auth')
def api_auth():
    user = g.user
    if user is None:
        return redirect(url_for('login'))

    u_k = UserKeys.query.filter_by(email=user.email).first()
    api_key = u_k.api_key
    secret = u_k.secret
    p = poloniex.poloniex(str(api_key), str(secret))
    balances = p.returnBalances()
    coins = CoinsConvList.query.filter_by(enable=True).all()
    coins_set = set(map(lambda coin: coin.name[4:], coins))
    currencies = ConvertUsd.query.filter_by(enable=True).all()
    cur_set = set(map(lambda cur: cur.coins, currencies))
    coins_set = coins_set.union(cur_set)
    balances = [(k, v) for k, v in balances.items() if k in coins_set]

    return render_template('/api-auth.html',
                           user=user,
                           api_key=type(api_key),
                           secret=secret,

                           balances=balances)
    """
    req={}
    command = 'returnBalances'
    req['command'] = command
    req['nonce'] = int(time.time()*1000)
    post_data = urllib.urlencode(req)
    sign = hmac.new(Secret, post_data, hashlib.sha512).hexdigest()
    headers = {
        'Sign': sign,
        'Key': APIKey
    }

    ret = urllib2.urlopen(urllib2.Request('https://poloniex.com/tradingApi', post_data, headers))
    jsonRet = json.loads(ret.read())
    return str(jsonRet)
    """