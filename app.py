#!flask/bin/python
import os, datetime, requests, time, json
from flask import Flask, jsonify, abort, make_response, request, render_template
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand


app = Flask(__name__, static_folder='static', static_url_path='')
app.config.from_object('config')
db = SQLAlchemy(app)
admin = Admin(app, url="/admin")
migrate = Migrate(app, db)
manager = Manager(app)
manager.add_command('db', MigrateCommand)


class Variables(db.Model):
    __tablename__ = 'variables'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200))
    title = db.Column(db.String(200))
    boolean = db.Column(db.Boolean)
    number = db.Column(db.Integer)
    text = db.Column(db.Text)
    
    def __repr__(self):
        return "<Variables({})>".format(self.name)


class CoinsConvList(db.Model):
    __tablename__ = 'coins_conv_list'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(200))
    enable = db.Column(db.Boolean)
    
    def __repr__(self):
        return "<CoinsConvList({})>".format(self.name)


class ConvertUsd(db.Model):
    __tablename__ = 'convert_usd'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    coins = db.Column(db.String(200))
    usd = db.Column(db.String(200))
    enable = db.Column(db.Boolean)
    
    def __repr__(self):
        return "<ConvertUsd({})>".format(self.coins)

class User(db.Model):
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    f_name = db.Column(db.String(200))
    l_name = db.Column(db.String(200))
    email = db.Column(db.String(200), unique=True)
    password = db.Column(db.String(200))
    user_keys = db.relationship('UserKeys', backref='user', lazy='dynamic')

    def __repr__(self):
        return "{}".format(self.email)

class UserKeys(db.Model):
    __tablename__ = 'user_secret_keys'
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    title = db.Column(db.String(200))
    api_key = db.Column(db.String(250))
    secret = db.Column(db.String(300))
    email = db.Column(db.String(200))
    enable = db.Column(db.Boolean)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return 'User_id:{0} \n Secret: {1}'.format(self.user_id, self.secret)


admin.add_view(ModelView(Variables, db.session))
admin.add_view(ModelView(CoinsConvList, db.session))
admin.add_view(ModelView(ConvertUsd, db.session))
admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(UserKeys, db.session))

from views import *


if __name__ == '__main__':
    manager.run()