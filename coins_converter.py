import requests
from decimal import Decimal

'''
Keys from json:

.//div[@id='marketsContainer'] = last - rate of coins or currencies from MARKETS

.//div[@class='cols'] = highestBid - rate from SELL COINS with BTC or ETH
                        lowestAsk - rate from BUY COINS with BTC or ETH
'''

url = 'https://poloniex.com/public'
params = {'command': 'returnTicker'}
coins_conv_list = ['BTC_LSK', 'ETH_LSK', 'BTC_DAO', 'ETH_DAO']
convert_usd = {'BTC': 'USDT_BTC', 'ETH': 'USDT_ETH', 'XMR': 'USDT_XMR'}
amount_of_coins = 5


def get_currencies_rates(url, **params):
    '''
    get rates of currencies in json
    :param url: api request
    :param params: parameters for get api request
    :return: dict of currencies
    '''
    r = requests.get(url, **params)
    return r.json()


def convert_into_usd(var_coins_conv_list, var_convert_usd, dict_rates):
    coins_dict = {coin[coin.index('_') + 1:]: {} for coin in var_coins_conv_list}
    for coin in var_coins_conv_list:
        # coins which we can convert through other coins into USDT
        if coin[:4] != 'USDT':
            coins_dict[coin[4:]][coin[:3]] = round(Decimal(dict_rates[coin]['highestBid']) \
                                                   * Decimal('0.9975') \
                                                   * Decimal(dict_rates[var_convert_usd[coin[:3]]]['highestBid']) \
                                                   * Decimal('0.9975'), 8)

        # currencies which is allowed convert into USDT immediately
        else:
            coins_dict[coin[5:]][coin[:4]] = round(Decimal(dict_rates[coin]['highestBid']) \
                                                   * Decimal('0.9975'), 8)

    # print coins_dict
    return coins_dict


def coins_rate(coins_dict):
    for key_d in coins_dict:
        for key in ['BTC', 'ETH', 'XMR', 'USDT']:
            if key not in coins_dict[key_d]:
                coins_dict[key_d][key] = None
    # print coins_dict
    return coins_dict


def calc_quantity(price, quantity=1):
    '''
    this code I haven't used yet =)
    :param price:
    :param quantity:
    :return:
    '''
    for key, vel in price.items():
        price[key] = round(vel * quantity, 8)
    return price
