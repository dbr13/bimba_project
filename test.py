import requests
import json
from app import *
from decimal import Decimal


def convert_into_usd(coins_conv_list, convert_usd, dict_rates):
    coins_dict = {coin[coin.index('_')+1:]: {} for coin in coins_conv_list}
    for coin in coins_conv_list:
        if coin[:4] != 'USDT':
            coins_dict[coin[4:]][coin[:3]] = round(Decimal(dict_rates[coin]['highestBid']) \
                                           * Decimal('0.9975') \
                                           * Decimal(dict_rates[convert_usd[coin[:3]]]['highestBid']) \
                                           * Decimal('0.9975'), 8)
        else:
            coins_dict[coin[5:]][coin[:4]] = round(Decimal(dict_rates[coin]['highestBid']) \
                                               * Decimal('0.9975'), 8)

    #print coins_dict
    return coins_dict

def coins_rate(coins_dict):
    for key_d in coins_dict:
        for key in ['BTC', 'ETH', 'XMR', 'USDT']:
            if key not in coins_dict[key_d]:
                coins_dict[key_d][key] = None
    print coins_dict
    return coins_dict

url = 'https://poloniex.com/public'
params = {'command': 'returnTicker'}

r = requests.get(url=url, params=params)
dict_rates = r.json()

var_coins_conv_list = CoinsConvList.query.filter_by(enable=True).all()
coins_conv_list = list(map(lambda x: x.name, var_coins_conv_list))
var_convert_usd = ConvertUsd.query.filter_by(enable=True).all()
convert_usd = dict(map(lambda x: [x.usd, x.coins], var_convert_usd))

# print dict_rates
# print 'coins_list {}'.format(coins_conv_list)

# print 'coinv_usd {}'.format(convert_usd)



price = convert_into_usd(coins_conv_list=coins_conv_list,
                 convert_usd=convert_usd,
                 dict_rates=dict_rates)

coins_rate(price)
