from flask_wtf.form import Form
from wtforms import PasswordField, StringField
from wtforms.validators import Length, Email, DataRequired

class KeyForm(Form):
    title = StringField('Title', validators=[DataRequired()])
    api_key = StringField('APIKey', validators=[DataRequired(), Length(min=35, max=35)])
    secret = StringField('Secret', validators=[DataRequired()])
    email = StringField('Email', validators=[Email()])

class RegistrationForm(Form):
    f_name = StringField('First Name', validators=[DataRequired()])
    l_name = StringField('Last Name', validators=[DataRequired()])
    email = StringField('Email', validators=[Email()])
    password = PasswordField('Password', validators=[DataRequired()])

class LoginForm(Form):
    email = StringField('Email', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])