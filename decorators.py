from functools import wraps

from flask import g, flash, redirect, url_for, request
from app import *
from werkzeug.security import check_password_hash


def requires_user(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        #key = KeyForm
        #user = User.query.filter_by(email=key.email.data).first()
        if g.user is None:
            flash(u'User should be registered ')
            return redirect(url_for('login'))
        #elif user.email==key.email.data and check_password_hash(user.password, key.password.data):
        return func(*args, **kwargs)
    return wrapper
